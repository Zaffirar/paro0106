#include "gtest/gtest.h"
#include "scrabble.hpp"

struct ScrabbleTestSuite {};

TEST(ScrabbleTestSuite, BasicTest)
{
  ASSERT_EQ(Scrabble("A"),1);
  ASSERT_EQ(Scrabble("B"),3);
  ASSERT_EQ(Scrabble("C"),3);
  ASSERT_EQ(Scrabble("D"),2);
  ASSERT_EQ(Scrabble("E"),1);
  ASSERT_EQ(Scrabble("F"),4);
  ASSERT_EQ(Scrabble("G"),2);
  ASSERT_EQ(Scrabble("H"),4);
  ASSERT_EQ(Scrabble("I"),1);
  ASSERT_EQ(Scrabble("J"),8);
  ASSERT_EQ(Scrabble("K"),5);
  ASSERT_EQ(Scrabble("L"),1);
  ASSERT_EQ(Scrabble("M"),3);
  ASSERT_EQ(Scrabble("N"),1);
  ASSERT_EQ(Scrabble("O"),1);
  ASSERT_EQ(Scrabble("P"),3);
  ASSERT_EQ(Scrabble("Q"),10);
  ASSERT_EQ(Scrabble("R"),1);
  ASSERT_EQ(Scrabble("S"),1);
  ASSERT_EQ(Scrabble("T"),1);
  ASSERT_EQ(Scrabble("U"),1);
  ASSERT_EQ(Scrabble("V"),4);
  ASSERT_EQ(Scrabble("W"),4);
  ASSERT_EQ(Scrabble("X"),8);
  ASSERT_EQ(Scrabble("Y"),4);
  ASSERT_EQ(Scrabble("Z"),10);
}

TEST(ScrabbleTestSuite, ShortWordTest)
{
  ASSERT_EQ(Scrabble("PIG"), 6);
  ASSERT_EQ(Scrabble("CABBAGE"), 14);
}

TEST(ScrabbleTestSuite, MoreWords)
{
  ASSERT_EQ(Scrabble("PIGGY"), 12);
  ASSERT_EQ(Scrabble("QA"), 11);
  ASSERT_EQ(Scrabble("QUESTION"), 17);
  ASSERT_EQ(Scrabble("BIGBOSS"), 12);
}
