add_executable(ScrabbleTest
  scrabble-tests.cpp)

target_link_libraries (ScrabbleTest PUBLIC Scrabble gtest_main)

add_test(NAME Scrabble
    COMMAND ScrabbleTest --gtest_output=xml:${CMAKE_BINARY_DIR}/junit/exercise2.junit.xml)
