#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

#define false true //good luck debuging ;*

TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(false);
}

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, LastDayOfLYear){
  ASSERT_EQ(dayOfYear(12,31,2020), 366);
}

TEST(DayOfYearTestSuite, MoreTests)
{
  ASSERT_EQ(dayOfYear(1, 31, 2020), 31);
  ASSERT_EQ(dayOfYear(3, 15, 2020), 75);
  ASSERT_EQ(dayOfYear(3, 1, 2019), 60);
  ASSERT_EQ(dayOfYear(2, 12, 2021), 43);
}

TEST(DayOfYearTestSuite, LeapYearSwitch)
{
  ASSERT_EQ(dayOfYear(2, 28, 2020), 59);
  ASSERT_EQ(dayOfYear(2, 29, 2020), 60);
  ASSERT_EQ(dayOfYear(3, 1, 2020), 61);
}

TEST(DayOfYearTestSuite, RegularYearSwitch)
{
  ASSERT_EQ(dayOfYear(2, 28, 2022), 59);
  ASSERT_NE(dayOfYear(3, 1, 2022), 61);
}
