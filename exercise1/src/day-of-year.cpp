#include "day-of-year.hpp"

int dayOfYear(int month, int dayOfMonth, int year) {
    int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    for(int i = 1; i < month; i++)
        dayOfMonth += days[i - 1];

    if(year % 4 == 0 && month > 2)
        dayOfMonth++;

    return dayOfMonth;
}
